Launching the GUI
=================
.. _launch-gui-target:

Quick Start
-----------

The GUI is based on Jupyter notebooks. You can get a new notebook to run the GUI in,
by running the follwing:

.. code-block:: shell

   $ clease-gui new

This creates a new notebook called ``clease_gui.ipynb``. (Alternatively, you can give it a different
name with the ``-f`` flag). Then make a new jupyter lab server,

.. code-block:: shell

   $ jupyter-lab

Open the notebook file you just created, and start using the GUI.

Manually Setting up a Notebook
-------------------------------

**Alternatively**, you can manually create a new notebook that you want to work in,
and add the following lines in an empty cell:

.. code-block:: python

   %matplotlib tk

   from clease_gui import display_ui
   display_ui();
