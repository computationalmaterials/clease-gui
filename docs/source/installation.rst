Installation
============

PyPI
-----
The CLEASE GUI can be installed from `PyPI <https://pypi.org/project/clease-gui/>`_:

.. code-block:: sh

    pip install clease-gui

Anaconda
--------

The GUI is also available with anaconda on `conda-forge <https://anaconda.org/conda-forge/clease-gui>`_.
To install into your conda environment, do:

.. code-block:: sh

    $  conda install -c conda-forge clease-gui

.. note:: This is the preferred method on Windows, due to the depdency on CLEASE.
    For more information, see `CLEASE docs <https://clease.readthedocs.io/en/stable/#installation>`_.

Directly from GitLab
---------------------

Alternatively, it can be installed directly from the master branch via GitLab:

.. code-block:: sh

    pip install git+https://gitlab.com/computationalmaterials/clease-gui.git@master
