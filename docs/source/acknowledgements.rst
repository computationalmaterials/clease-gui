Partners and Support
=====================

Development of the CLEASE GUI was supported by `BIG-MAP <https://www.big-map.eu/>`_ (H2020, #957189).

.. image:: ./_static/bigmap.png
    :target: https://www.big-map.eu/
    :width: 200
    :align: center
