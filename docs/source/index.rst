.. CLEASE GUI documentation master file, created by
   sphinx-quickstart on Fri Aug 20 14:59:22 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==========
CLEASE GUI
==========

This is a graphical user interface (GUI) based on Jupyter notebooks for the
Cluster Expansion (CE) code `CLEASE <https://gitlab.com/computationalmaterials/clease>`_.

The GUI has capabilities of doing several of the standard things you would normally
want to use CLEASE for. However, there are still many features of CLEASE which are
(currently) not implemented in the GUI.

Contact
-------
If you encounter any issues or would like to request features,
please don't hesitate to reach out on our
`GitLab issues <https://gitlab.com/computationalmaterials/clease-gui/-/issues>`_
page.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   ./installation
   ./launching
   ./examples/examples
   ./acknowledgements
