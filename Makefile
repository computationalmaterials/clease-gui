.PHONY: publish_test publish clean package

COMMAND=python
ifdef PYCMD
	COMMAND=$(PYCMD)
endif

clean:
	rm -rf dist/
	rm -rf build/
	rm -rf *.egg-info/
	pyclean -v .

package:
	$(COMMAND) -m build

check:
	twine check dist/*

publish_test:
	make clean
	make package
	make check
	twine upload --repository testpypi dist/*

publish:
	make clean
	make package
	make check
	twine upload --verbose dist/*
