import pytest

from clease_gui.app_data import Notification, AppDataKeys, AppData, Notifier


@pytest.fixture
def app_data_keys():
    return AppDataKeys


@pytest.fixture
def data():
    return AppData()


def test_app_data_keys(app_data_keys):
    assert all(isinstance(key.value, str) for key in app_data_keys)


def test_public_keys(app_data_keys):
    public = list(app_data_keys.iter_public_keys())
    assert len(app_data_keys) > len(public)

    for key in public:
        assert not key.startswith("_")
        assert not key.value.startswith("_")
        assert app_data_keys.is_key_public(key)


def test_private_keys(app_data_keys):
    private = list(app_data_keys.iter_private_keys())
    assert len(app_data_keys) > len(private)

    for key in private:
        assert key.startswith("_")
        assert app_data_keys.is_key_private(key)


def test_public_private(app_data_keys):
    public = list(app_data_keys.iter_public_keys())
    private = list(app_data_keys.iter_private_keys())

    assert len(app_data_keys) == len(public) + len(private)
    assert set(public).union(private) == set(app_data_keys)
    assert len(set(public)) == len(public)
    assert len(set(private)) == len(private)


def test_subscribe(data, mocker):
    class DummyClass:
        @staticmethod
        def myfun(change):
            assert isinstance(change, Notification)

    foo = DummyClass()
    spy = mocker.spy(foo, "myfun")

    assert spy.call_count == 0

    key = "my_key"
    data.subscribe(key, foo.myfun)
    assert spy.call_count == 0
    data[key] = 2
    assert spy.call_count == 1
    assert data[key] == 2
    assert spy.call_count == 1

    data[key] = 3
    assert spy.call_count == 2
    assert data[key] == 3
    assert spy.call_count == 2

    del data[key]
    assert spy.call_count == 3


def test_notification_set(data, mocker):
    class DummyClass:
        @staticmethod
        def myfun(change):
            assert change.notifier == Notifier.SET

    foo = DummyClass()
    spy = mocker.spy(foo, "myfun")
    key = "my_key"
    data.subscribe(key, foo.myfun)

    assert spy.call_count == 0
    data[key] = 1
    assert spy.call_count == 1


def test_notification_del(data, mocker):
    class DummyClass:
        @staticmethod
        def myfun(change):
            assert change.notifier == Notifier.DELETE

    foo = DummyClass()
    spy = mocker.spy(foo, "myfun")
    key = "my_key"
    data[key] = 2
    data.subscribe(key, foo.myfun)

    assert spy.call_count == 0
    del data[key]
    assert spy.call_count == 1


def test_in(data):
    assert "settings" not in data
    data["settings"] = object()
    assert "settings" in data
    data.pop("settings")
    assert "settings" not in data


def test_pop(data):
    with pytest.raises(KeyError):
        data.pop("foo")
    r = data.pop("foo", "bar")
    assert r == "bar"
    data["foo"] = "baz"
    assert "foo" in data
    r = data.pop("foo", "bar")
    assert r == "baz"
    assert "foo" not in data
    r = data.pop("foo", None)
    assert r is None
