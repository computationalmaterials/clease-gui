import pytest
import ipywidgets as widgets
from clease_gui import utils


@pytest.fixture
def button():
    return widgets.Button()


def test_disable_widget_context(button):
    assert button.disabled is False
    with utils.disable_widget_context(button):
        assert button.disabled
    assert button.disabled is False


@pytest.mark.parametrize("inp", [1, "a_string", None])
def test_disable_widget_wrong_type(inp):
    with pytest.raises(TypeError):
        with utils.disable_widget_context(inp):
            pass


def test_disable_widget_decorator(button):
    @utils.disables_widget(button)
    def func():
        return button.disabled

    assert button.disabled is False
    assert func() is True


def test_already_disabled_button(button):
    button.disabled = True

    @utils.disables_widget(button)
    def func():
        return button.disabled

    assert button.disabled is True
    assert func() is True
    assert button.disabled is True


def test_disable_widget_cls_decorator(button):
    class MyCls:
        def __init__(self, button):
            self.button = button

        @utils.disable_cls_widget("button")
        def func(self):
            return self.button.disabled

        @utils.disable_cls_widget("not-a-button")
        def bad_func(self):
            return

    my_cls = MyCls(button)

    assert button.disabled is False
    assert my_cls.func() is True

    # Test we cannot disable a button which isn't defined in self
    with pytest.raises(AttributeError):
        my_cls.bad_func()
