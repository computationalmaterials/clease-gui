import pytest
from ase.db import connect
from clease_gui import new_structures


@pytest.fixture
def make_dashboard(make_app_data, make_test_settings):
    def _make_dashboard(settings=None):
        if settings is None:
            settings = make_test_settings()
        app_data = make_app_data(settings=settings)
        return new_structures.NewStructureDashboard(app_data)

    return _make_dashboard


def test_get_generator_settings(make_dashboard):
    dashboard = make_dashboard()
    gen_settings = dashboard.get_generator_settings()
    assert gen_settings["func_name"] == "generate_initial_pool"

    assert len(gen_settings["args"]) == 0
    assert "atoms" in gen_settings["kwargs"]


@pytest.mark.parametrize(
    "scheme, expected_func",
    [
        ("initial_pool", "generate_initial_pool"),
        ("random_structure", "generate_random_structures"),
        ("ground_state_structure", "generate_gs_structure"),
        ("probe_structure", "generate_probe_structure"),
    ],
)
@pytest.mark.parametrize("num", [3, 4])
def test_do_generate(
    num,
    scheme,
    expected_func,
    make_dashboard,
    make_populated_test_settings,
    get_num_precalculated,
    mock_exist_in_db,
    set_seed,
):
    if scheme == "probe_structure":
        pytest.skip("Too slow, currently")
    settings = None
    expected_start_num = 1
    if scheme == "probe_structure":
        # We need to populate some structures in the database
        settings = make_populated_test_settings()
        expected_start_num = get_num_precalculated()

    dashboard = make_dashboard(settings=settings)
    # Note: We cannot test probe-structures here, as that requires a populated DB
    if scheme in ["ground_state_structure", "probe_structure"]:
        # GS needs some ECI
        fake_eci = {"c0": 0.0}
        dashboard.app_data[dashboard.KEYS.ECI] = fake_eci
    db_name = dashboard.settings.db_name
    # This dashboard has the widgets to set the generation schemas
    gen_dash = dashboard.general_new_struct_dashboard
    gen_dash.struct_per_gen_b.value = num

    dashboard.struct_gen_dashboard.available_schemes_b.value = scheme

    gen_settings = dashboard.get_generator_settings()
    assert expected_func == gen_settings["func_name"]

    con = connect(db_name)

    # Check the DB only has 1 entry, nothing as been added yet
    assert sum(1 for _ in con.select()) == expected_start_num

    dashboard._do_generate()

    assert sum(1 for _ in con.select()) == expected_start_num + num
