import pytest


@pytest.fixture
def mock_exist_in_db(mocker):
    """Patch the structure comparison method in NewStructures class,
    such that it always returns False, i.e. it never does structure comparison"""

    def patched_func(*args, **kwargs):
        # Always say that the structure is new, and never do an actual comparison,
        # no matter the input
        return False

    mocker.patch("clease.structgen.NewStructures._exists_in_db", patched_func)
