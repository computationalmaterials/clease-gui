import pytest
from pathlib import Path
import numpy as np
from clease_gui.main_dashboard import MainDashboard
import clease_gui as gui
from clease_gui.main import BaseGUI
from clease_gui.app_data import AppData


@pytest.fixture
def dashboard():
    return MainDashboard(AppData())


@pytest.fixture
def app():
    return BaseGUI()


def test_app_initialization(app):
    assert isinstance(app.app_data, AppData)
    assert isinstance(app.dashboard, MainDashboard)
    app.display()


def test_initialization(dashboard):
    assert hasattr(dashboard, "app_data")
    assert isinstance(dashboard.app_data, AppData)


def test_cwd(dashboard):
    cur_cwd = Path(".").resolve()
    assert dashboard.cwd == cur_cwd
    newdir = "/some/dir"
    dashboard.set_cwd(newdir)

    assert dashboard.cwd == Path(newdir).resolve()

    # Test changing cwd using widget
    newdir2 = Path("/some/other/directory").resolve()
    wdgt = dashboard.cwd_widget
    wdgt.value = str(newdir2)
    assert dashboard.cwd == newdir2


def test_save_load_app_state(dashboard):
    """Test saving and loading of app data in a
    round trip"""
    # Populate some data in the app data
    app_data = dashboard.app_data

    assert gui.AppDataKeys.CWD in app_data
    assert gui.AppDataKeys.STATUS in app_data

    lst = [1, 2, 3]
    arr = np.array([6, 7, 8])
    app_data["tester"] = lst
    app_data["nparray"] = arr

    fname = "my_file.json"
    dashboard._save_app_data(fname)

    app_data.clear()
    assert dashboard.app_data == {}

    dashboard._load_app_data(fname)
    # We should not have saved any data on CWD or STATUS keys
    assert gui.AppDataKeys.CWD not in app_data
    assert gui.AppDataKeys.STATUS not in app_data

    assert "tester" in app_data
    assert "nparray" in app_data

    assert app_data["tester"] == lst
    assert (app_data["nparray"] == arr).all()
