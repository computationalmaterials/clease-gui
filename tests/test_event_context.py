import pytest
from clease_gui.event_context import EventContextManager


@pytest.mark.parametrize(
    "error",
    [
        Exception,
        ValueError,
        TypeError,
        ZeroDivisionError,
        RuntimeError,
        NameError,
        OSError,
    ],
)
def test_event_context(error):
    """Test the event context suppresses expected error types"""
    with EventContextManager() as cm:
        raise error()
        # This should be unreachable
        raise BaseException("Reached unreachable code!")
    assert isinstance(cm.exc_value, error)
    assert cm.success is False
    assert cm.is_done is True


@pytest.mark.parametrize(
    "error",
    [
        BaseException,
        KeyboardInterrupt,
        SystemExit,
        GeneratorExit,
    ],
)
def test_event_context_no_suppress(error):
    """Test exceptions we shouldn't suppress"""
    with pytest.raises(error):
        with EventContextManager():
            raise error()


def test_event_success():
    with EventContextManager() as cm:
        pass
    assert cm.success
    assert cm.had_error is False


def test_is_done():
    with EventContextManager() as cm:
        assert cm.is_done is False
    assert cm.is_done is True
    assert cm.success
    assert cm.exc_type is None
