import shutil
import random
import pytest
from pathlib import Path
import ase
from ase.db import connect
from ase.calculators.calculator import compare_atoms
import numpy as np
import matplotlib.pyplot as plt
import clease
import clease_gui as gui
from clease_gui.app_data import AppData


@pytest.fixture
def rng():
    """Make a numpy RNG generator, with a fixed seed for reproducibility"""
    return np.random.default_rng(42)


@pytest.fixture(autouse=True)
def set_seed():
    np.random.seed(18)
    random.seed(18)


@pytest.fixture(autouse=True)
def _plt_close_figures():
    """Automatically close matplotlib figures.
    Adapted from the ASE test suite."""
    yield
    fignums = plt.get_fignums()
    for fignum in fignums:
        plt.close(fignum)


@pytest.fixture(autouse=True)
def auto_tmp_workdir(tmp_path):
    """Fixture to automatically switch working directory of the test
    to the tmp path.
    Will automatically be used, so it does not need to be used explicitly
    by the test.
    """
    path = Path(str(tmp_path))  # Normalize path
    with ase.utils.workdir(path, mkdir=True):
        yield


@pytest.fixture(autouse=True)
def auto_mock_update_statusbar(mocker):
    """Mock the update_statusbar wrapper"""

    def dummy_wrapper(func):
        def _wrapper(self, *args, **kwargs):
            return func(self, *args, **kwargs)

        return _wrapper

    mocker.patch("clease_gui.status_bar.update_statusbar", dummy_wrapper)


@pytest.fixture
def make_app_data():
    """Fixture to make (and populate) a dummy app data dictionary.
    Contains the CWD key by default.
    """

    def _make_app_data(settings=None, **kwargs):
        cwd = Path(".").resolve()
        app_data = {
            gui.AppDataKeys.CWD: cwd,
        }
        app_data.update(**kwargs)

        if settings:
            app_data[gui.AppDataKeys.SETTINGS] = settings

        return AppData(**app_data)

    return _make_app_data


@pytest.fixture
def make_conc():
    def _factory(basis_elements=None, **kwargs):
        if basis_elements is None:
            basis_elements = [["Au", "Cu"]]
        return clease.settings.Concentration(basis_elements=basis_elements, **kwargs)

    return _factory


@pytest.fixture
def make_test_settings(make_conc):
    """Factory for creating a settings with CEBulk"""
    # Default fallback settings, if "settings_overwrites" is not defined
    default_settings = dict(
        crystalstructure="fcc",
        a=3.8,
        max_cluster_dia=[4.0],
    )

    def _factory(basis_elements=None, conc_kwargs=None, settings_overwrites=None, **kwargs):
        conc_kwargs = conc_kwargs or {}
        settings_dct = settings_overwrites or default_settings.copy()
        conc = make_conc(basis_elements=basis_elements, **conc_kwargs)

        settings_dct.update(kwargs)
        settings = clease.settings.CEBulk(conc, **settings_dct)
        return settings

    return _factory


def get_precalculated_db():
    path = Path(__file__).parent / "precalculated_aucu.db"
    return path


@pytest.fixture
def get_num_precalculated():
    """Get number of entries in the pre-calculated database"""

    def _get_num():
        with connect(get_precalculated_db()) as con:
            return sum(1 for _ in con.select())

    return _get_num


@pytest.fixture
def make_populated_test_settings(make_test_settings):
    """Populate the settings database with some pre-calculated database.
    DB is calculated on AuCu with default settings, using an iniitial pool with an EMT calculator.
    No relaxation.
    """

    def _maker():
        settings = make_test_settings()
        src = get_precalculated_db()
        dst = Path(".") / settings.db_name

        shutil.copyfile(src, dst)
        return settings

    return _maker


@pytest.fixture
def atoms_equal():
    """Fixture to compare equality between two atoms objects.
    Returns True if the two atoms are the same
    """

    def _do_compare(atoms1, atoms2, tol=1e-8):
        return len(compare_atoms(atoms1, atoms2, tol=1e-8)) == 0

    return _do_compare
