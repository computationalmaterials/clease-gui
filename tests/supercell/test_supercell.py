from pathlib import Path
import pytest
from ase.io import write, read
from clease_gui.supercell import SupercellDashboard
from clease_gui.app_data import AppDataKeys


@pytest.fixture
def settings(make_test_settings):
    """Standard settings fixture"""
    return make_test_settings()


@pytest.fixture
def atoms(settings, rng):
    """Make an atoms object with mixed Au/Cu"""
    atoms = settings.prim_cell.copy() * (9, 9, 9)
    atoms.symbols = "Au"
    natoms = len(atoms)
    atoms.symbols[: (natoms // 2)] = "Cu"
    # Randomize the order
    syms = list(atoms.symbols)
    rng.shuffle(syms)
    atoms.symbols = syms
    return atoms


@pytest.fixture
def dashboard(settings, make_app_data):
    app_data = make_app_data(settings=settings)
    return SupercellDashboard(app_data)


def test_initialize(dashboard):
    for name in ("save_supercell_button", "sc_widgets", "view_atoms_btn"):
        assert hasattr(dashboard, name)


def test_load_supercell(dashboard, atoms, atoms_equal):

    fname = "tester.traj"
    write(fname, atoms)

    app_data = dashboard.app_data
    key = AppDataKeys.SUPERCELL
    assert key not in app_data

    dashboard._load_supercell(fname)

    assert key in app_data
    atoms_loaded = app_data[key]

    assert atoms_equal(atoms, atoms_loaded)

    del app_data[key]

    dashboard.load_supercell_text.value = fname
    assert key not in app_data
    dashboard.load_supercell()
    assert key in app_data
    atoms_loaded = app_data[key]

    assert atoms_equal(atoms, atoms_loaded)


def test_save_supercell(dashboard, atoms, atoms_equal):
    file = Path(dashboard.save_supercell_text.value)
    assert not file.exists()
    dashboard.set_supercell(atoms)
    dashboard.save_supercell()
    assert file.exists()
    loaded = read(file)

    assert atoms_equal(atoms, loaded)


def test_get_energy_btn(dashboard, settings, atoms):
    assert dashboard.get_energy_btn.disabled is True
    dashboard.app_data[AppDataKeys.SETTINGS] = settings

    assert dashboard.get_energy_btn.disabled is True
    dashboard.app_data[AppDataKeys.ECI] = {"c0": -0.5}

    assert dashboard.get_energy_btn.disabled is True
    dashboard.app_data[AppDataKeys.SUPERCELL] = atoms

    # Now we have all 3 componenets
    assert dashboard.get_energy_btn.disabled is False

    assert isinstance(dashboard.get_energy_of_supercell(), float)
