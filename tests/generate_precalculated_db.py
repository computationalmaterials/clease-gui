import clease
from ase.db import connect
from ase.calculators.emt import EMT


def main():
    """Create the precalculated Au Cu database for testing using unrelaxed EMT"""
    db_name = "precalculated_aucu.db"
    settings_dct = {
        "crystalstructure": "fcc",
        "a": 3.8,
        "db_name": db_name,
        "supercell_factor": 27,
    }
    conc = clease.settings.Concentration(basis_elements=[["Au", "Cu"]])

    settings = clease.settings.CEBulk(conc, **settings_dct)

    ns = clease.structgen.NewStructures(settings, struct_per_gen=40)
    ns.generate_initial_pool()

    con = connect(db_name)
    for row in con.select(gen=0):
        calc = EMT()
        atoms = row.toatoms()
        atoms.calc = calc
        atoms.get_potential_energy()

        clease.tools.update_db(uid_initial=row.id, final_struct=atoms, db_name=db_name)


if __name__ == "__main__":
    main()
