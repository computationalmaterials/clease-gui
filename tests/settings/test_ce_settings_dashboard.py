from clease.settings.settings import ClusterExpansionSettings
import pytest
from clease_gui.settings_maker import CESettingsDashboard
import clease_gui as gui


@pytest.fixture
def make_dashboard(make_app_data):
    def _make_dashboard(settings=None):
        app_data = make_app_data()
        if settings:
            app_data[gui.AppDataKeys.SETTINGS] = settings
        return CESettingsDashboard(app_data)

    return _make_dashboard


@pytest.fixture
def dashboard(make_dashboard, settings):
    """Fixture to make dashboard with settings object for simplicity"""
    return make_dashboard(settings=settings)


def test_initialization(make_dashboard, settings):
    dashboard = make_dashboard()
    assert hasattr(dashboard, "app_data")
    # No settings are present yet
    with pytest.raises(KeyError):
        dashboard.settings
    dashboard = make_dashboard(settings=settings)
    assert isinstance(dashboard.settings, ClusterExpansionSettings)


def test_update_settings(dashboard):

    kwargs = dashboard.get_settings_kwargs()
    assert pytest.approx(kwargs["max_cluster_dia"]) == [5, 5, 5]
    dashboard.update_settings()
    assert dashboard.settings.max_cluster_dia == pytest.approx([5, 5, 5])

    dashboard.common_settings_widget.max_cluster_size.value = 3
    kwargs = dashboard.get_settings_kwargs()
    assert pytest.approx(kwargs["max_cluster_dia"]) == [5, 5]

    # Settings MCD should still be [5, 5, 5]
    assert dashboard.settings.max_cluster_dia == pytest.approx([5, 5, 5])
    dashboard.update_settings()
    # Now the settings should've updated
    assert dashboard.settings.max_cluster_dia == pytest.approx([5, 5])


def test_set_widgets_on_load(make_dashboard, settings):

    dashboard = make_dashboard()

    exp = pytest.approx(settings.max_cluster_dia)

    kwargs = dashboard.get_settings_kwargs()
    assert exp != kwargs["max_cluster_dia"]
    dashboard.set_widgets_from_load(settings)
    kwargs = dashboard.get_settings_kwargs()
    assert exp == kwargs["max_cluster_dia"]
    assert exp == settings.max_cluster_dia
