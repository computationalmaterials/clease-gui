import pytest
from clease.settings import ClusterExpansionSettings
import clease_gui as gui


@pytest.fixture
def dashboard(settings, make_app_data):
    app_data = make_app_data(settings)
    return gui.settings_maker.ClusterDashboard(app_data)


def test_initialization(dashboard):
    # Test a couple of attributes exist
    for attr in ("app_data", "clusters_content_out", "refresh_table_button"):
        assert hasattr(dashboard, attr)


def test_get_settings(dashboard):
    assert isinstance(dashboard.settings, ClusterExpansionSettings)


def test_click_view(dashboard, mocker):
    spy = mocker.spy(ClusterExpansionSettings, "clusters_table")

    assert spy.call_count == 0
    dashboard.refresh_clusters_table()
    assert spy.call_count == 1
