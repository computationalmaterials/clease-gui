import pytest
import clease


@pytest.fixture
def conc():
    """Simple concentration object"""
    basis_elements = [["Au", "Cu"]]
    return clease.settings.Concentration(basis_elements=basis_elements)


@pytest.fixture
def default_kwargs():
    """Default keyword args for CLEASE settings"""
    kwargs = dict(a=4, crystalstructure="sc", max_cluster_dia=(5.0,))
    return kwargs


@pytest.fixture
def settings(conc, default_kwargs):
    return clease.settings.CEBulk(conc, **default_kwargs)
