import pytest
import clease_gui as gui


@pytest.fixture
def dashboard(make_app_data):
    app_data = make_app_data()
    return gui.settings_maker.ConcentrationDashboard(app_data)


def test_initialization(dashboard):
    # Test a couple of attributes exist
    for attr in ("app_data", "basis_elements"):
        assert hasattr(dashboard, attr)
    assert dashboard.basis_elements == []


def test_bad_elements(dashboard, mocker):
    basis = ["AU"]
    spy = mocker.spy(dashboard, "bad_basis_element")
    assert spy.call_count == 0
    dashboard.add_basis(basis)
    assert spy.call_count == 1
    assert dashboard.basis_elements == []


def test_add_basis(dashboard):
    assert dashboard.basis_elements == []
    basis1 = ["Au"]
    basis2 = ["Au", "Ag"]
    dashboard.add_basis(basis1)
    assert dashboard.basis_elements == [basis1]
    dashboard.add_basis(basis2)
    assert dashboard.basis_elements == [basis1, basis2]
