import pytest
from clease_gui import settings_maker
import clease_gui as gui


@pytest.fixture
def dashboard(make_app_data):
    app_data = make_app_data()
    return settings_maker.SettingsMakerDashboard(app_data)


def test_initialization(dashboard):
    assert hasattr(dashboard, "app_data")
    assert isinstance(dashboard.app_data, dict)
    assert hasattr(dashboard, "tab")
    assert dashboard.settings is None


def test_make_settings_uninitialized(dashboard):
    """We cannot make settings with no inputs"""
    with pytest.raises(ValueError):
        dashboard.make_settings()


def test_set_settings(dashboard, settings):
    app_data = dashboard.app_data
    assert gui.AppDataKeys.SETTINGS not in app_data
    dashboard.set_settings(settings)
    assert gui.AppDataKeys.SETTINGS in app_data
    assert dashboard.settings is settings
    assert app_data[gui.AppDataKeys.SETTINGS] is dashboard.settings


def test_save_load_roundtrip(dashboard, settings):
    dashboard.set_settings(settings)
    dashboard.save_settings("test.json")
    dashboard.load_settings("test.json")

    dashboard.make_settings()
    new_settings = dashboard.settings
    for key in {"max_cluster_size", "max_cluster_dia"}:
        new_val = getattr(new_settings, key)
        exp_val = getattr(settings, key)
        assert new_val == pytest.approx(exp_val)
