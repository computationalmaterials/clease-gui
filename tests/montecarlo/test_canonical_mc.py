import pytest
from clease_gui.montecarlo import CanonicalMC
from clease_gui import AppDataKeys


@pytest.fixture
def dashboard(app_data):
    return CanonicalMC(app_data)


@pytest.mark.parametrize("step_obs_val", ["off", False, True])
def test_mc_app_data(dashboard: CanonicalMC, step_obs_val):
    mc_data = dashboard.get_mc_data()
    dashboard.step_observer_wdgt.value = step_obs_val
    assert len(mc_data) == 0
    dashboard._run_mc()
    mc_data = dashboard.get_mc_data()
    temp = dashboard.get_temp()
    assert len(mc_data) == 1
    data = mc_data[0]
    assert len(data["accept_rate"]) == len(temp)
    assert len(data["emin"]) == len(temp)
    assert data["temperature"] == pytest.approx(temp)
    assert len(data["mean_energy"]) == len(temp)
    obs = dashboard.app_data[AppDataKeys.STEP_OBSERVER]
    if step_obs_val != "off":
        assert obs is not None
        assert len(obs.steps) > 0
    else:
        assert obs is None
