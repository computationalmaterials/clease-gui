import pytest
import random
from clease.settings import CEBulk, Concentration
from clease_gui.app_data import AppDataKeys


@pytest.fixture
def app_data(make_app_data):
    conc = Concentration(basis_elements=[["Au", "Cu"]])
    settings = CEBulk(conc, crystalstructure="fcc", a=4.0)

    sc = settings.prim_cell * (4, 4, 4)
    N = len(sc)
    sc.symbols = "Au"
    sc.symbols[: (N // 2)] = "Cu"
    syms = list(sc.symbols)
    random.shuffle(syms)
    sc.symbols = syms

    names = settings.all_cf_names
    eci = {name: random.uniform(-1, 1) for name in names}

    data = make_app_data(settings=settings)
    data[AppDataKeys.SUPERCELL] = sc
    data[AppDataKeys.ECI] = eci
    return data
