import subprocess
from pathlib import Path
import pytest

import clease_gui
from click.testing import CliRunner
from clease_gui.cli.main_cli import clease_gui_cli
from clease_gui.cli.notebook import DEFAULT_FILENAME, new
from clease_gui import cli


@pytest.fixture
def runner():
    return CliRunner()


@pytest.fixture
def run_cmd(runner):
    def _run_cmd(cli_cls, opts=None):
        result = runner.invoke(cli_cls, opts)
        assert result.exit_code == 0
        return result

    return _run_cmd


def test_entry_point():
    """Test the entry point from the install worked.
    Remaining tests are run with the click CliRunner"""
    result = subprocess.run("clease-gui --version", shell=True, check=True)
    assert result.returncode == 0


def test_version(run_cmd):
    opts = "--version"
    p = run_cmd(clease_gui_cli, opts=opts)
    assert clease_gui.__version__ in p.stdout


def test_new_notebook_default(run_cmd):
    filename = Path(DEFAULT_FILENAME)

    assert not filename.is_file()
    run_cmd(new)
    assert filename.is_file()


@pytest.mark.parametrize("filename", ["test", "test.ipynb", "test.txt"])
def test_new_notebook_filename(runner, run_cmd, filename):
    pa = Path(filename)
    expected = pa.with_suffix(".ipynb")

    assert not expected.is_file()
    opts = f"-f {filename}"
    run_cmd(new, opts=opts)
    assert expected.is_file()


def test_launch(run_cmd, mocker):
    def _mock_system(cmd):
        # print cmd to stdout
        print(cmd)

    mocker.patch("os.system", _mock_system)

    result = run_cmd(cli.launch)
    assert "jupyter-lab" in result.stdout
    assert "jupyter notebook" not in result.stdout

    result = run_cmd(cli.launch, opts="-n")
    assert "jupyter notebook" in result.stdout
    assert "jupyter-lab" not in result.stdout

    result = run_cmd(cli.launch, opts="-- my_arg")
    assert "my_arg" in result.stdout
    assert "jupyter-lab" in result.stdout

    result = run_cmd(cli.launch, opts="-n -- my_arg")
    assert "my_arg" in result.stdout
    assert "jupyter notebook" in result.stdout
