from typing import Sequence
import logging
from ase import Atoms
import ipywidgets as widgets
from IPython.display import clear_output, display

from clease_gui import register_logger
from clease_gui.utils import make_clickable_button, is_value_change


HAS_NGL = True
try:
    import nglview as nv
except ImportError:
    HAS_NGL = False

logger = logging.getLogger(__name__)
register_logger(logger)


def make_ngl_viewer(atoms: Atoms):
    if not HAS_NGL:
        raise RuntimeError("No nglview is installed. Try 'pip install nglview' first.")
    view = nv.show_ase(atoms, default=False)
    view.add_unitcell()
    view.add_spacefill()
    view.camera = "orthographic"
    view.parameters = {"clipDist": 0}

    view.center()
    view._view_height = "550px"

    view.update_spacefill(
        radiusType="covalent",
        radiusScale=0.4,
        color_scheme="element",
        color_scale="rainbow",
    )

    return view


class NGLViewGUI:
    def __init__(self, images: Sequence[Atoms]):

        self.viewers = [make_ngl_viewer(atoms) for atoms in images]
        self.images = images
        N = len(self.viewers)
        self.slider = widgets.IntSlider(min=0, max=N - 1, value=0)
        self.slider.observe(self._update_frame)
        self.refresh_btn = make_clickable_button(self.refresh_current_click, description="Refresh")
        self.redraw_btn = make_clickable_button(self.redraw_current, description="Redraw")
        self.next_btn = make_clickable_button(self.next, description="Next Image")
        self.prev_btn = make_clickable_button(self.prev, description="Previous Image")

        self.camera_states = {}

        refresh_wdgt = widgets.HBox([self.refresh_btn, self.redraw_btn])
        cntrl_wdgt = widgets.HBox([self.prev_btn, self.next_btn])
        self.viewer_output = widgets.Output(width="100%", height="550px")

        self.gui = widgets.VBox(
            [
                self.slider,
                cntrl_wdgt,
                refresh_wdgt,
                self.viewer_output,
            ]
        )

        # First update
        self._update_frame()
        fix_viewer(self.current_viewer)

    @property
    def current_index(self) -> int:
        return self.slider.value

    @property
    def current_viewer(self):
        return self.viewers[self.current_index]

    def _update_frame(self, change=None):
        if change is not None and not is_value_change(change):
            logger.debug("Ignoring event: %s", change)
            return
        logger.debug("Setting frame to index %d", self.current_index)

        if change is not None:
            # Remember the state of the viewer we just had
            # Since otherwise the camera state is always lost upon a redraw.
            old_id = change["old"]
            self._save_orientation(old_id)

        with self.viewer_output:
            clear_output(wait=True)
            display(self.current_viewer)
        # NGLview doesn't seem to properly update the orientation
        # upon re-displaying the frame, even though it remembers
        # its previous state.
        self._set_orientation(self.current_index)

    def _set_orientation(self, index: int) -> None:
        if index in self.camera_states:
            # We have previously visited this viewer. Set it to the remembered state.
            state = self.camera_states[index]
            self.viewers[index]._set_camera_orientation(state)

    def refresh_current_click(self, b) -> None:
        self.refresh_current()

    def refresh_current(self) -> None:
        viewer = self.current_viewer
        logger.debug("Refreshing viewer %d", self.current_index)
        fix_viewer(viewer)
        # Try to set the orientation if we stored that value.
        self._update_frame()

    def _save_orientation(self, index: int) -> None:
        frame = self.viewers[index]
        state = frame._camera_orientation
        self.camera_states[index] = state

    def redraw_current(self, b):
        idx = self.current_index
        # self._save_orientation(idx)
        new = make_ngl_viewer(self.images[idx])
        self.viewers[idx] = new
        fix_viewer(new)
        self._update_frame()

    def next(self, b):
        self.slider.value += 1

    def prev(self, b):
        self.slider.value -= 1
        self._set_orientation(self.current_index)


def toggle_camera(viewer):
    """Toggle camera between perspective and orthographic."""
    viewer.camera = "perspective" if viewer.camera == "orthographic" else "orthographic"


def fix_viewer(viewer):
    logger.debug("Handling resize")
    viewer.handle_resize()
    logger.debug("Toggle 1")
    toggle_camera(viewer)
    logger.debug("Toggle 2")
    toggle_camera(viewer)
