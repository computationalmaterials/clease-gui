import logging
from IPython.display import display, clear_output
import ipywidgets as widgets
import ase

from clease_gui.base_dashboard import BaseDashboard
from clease_gui.app_data import Notification
from clease_gui import register_logger
from .ngl_viewer import NGLViewGUI

__all__ = ["AtomsViewerDashboard"]

logger = logging.getLogger(__name__)
register_logger(logger)


class AtomsViewerDashboard(BaseDashboard):
    def initialize(self) -> None:
        self.atoms_output = widgets.Output(height="550px")
        self.viewer = None

        self.app_data.subscribe(self.KEYS.IMAGES, self.set_images)

    def display(self) -> None:
        super().display()
        display(self.atoms_output)

    def set_images(self, change: Notification) -> None:
        if change.key != self.KEYS.IMAGES:
            return
        images = change.new_value
        if isinstance(images, ase.Atoms):
            images = [images]
        viewer = NGLViewGUI(images)
        self.viewer = viewer
        with self.atoms_output:
            clear_output(wait=True)
            display(viewer.gui)
        logger.info("Images in the Atoms Viewer tab has been updated.")

    def on_selected_tab(self):
        if self.viewer is None:
            return

        logger.info("Refreshing current viewer")
        self.viewer.refresh_current()
